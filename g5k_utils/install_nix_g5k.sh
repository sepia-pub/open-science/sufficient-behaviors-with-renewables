#!/usr/bin/env bash
sudo-g5k
#creation of directory for nancy host
sudo mkdir -m 0755 -p /nix/var/nix/{profiles,gcroots}/per-user/$USER
sudo chown -R mmadon /nix

#nix installation
sh <(curl -L https://nixos.org/nix/install) --no-daemon --yes
. ~/.nix-profile/etc/profile.d/nix.sh

