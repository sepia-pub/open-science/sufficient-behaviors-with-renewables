#!/bin/bash
# launch the experiment with passive_submit.sh
# and automatically sync to save_directory every 10 minutes
while getopts w:s:e: flag
do
    # shellcheck disable=SC2220
    case "${flag}" in
        w) work_directory=${OPTARG};;
        s) save_directory=${OPTARG};;
        e) expe_directory=${OPTARG};;
    esac
done
echo "Work directory: ${work_directory}";
echo "Save directory: ${save_directory}";
echo "Expe directory: ${expe_directory}";
mkdir -p "${save_directory}";
chmod +x scripts/passive_submit.sh
scripts/passive_submit.sh "${expe_directory}" "${work_directory}" &
passive_pid=$!
chmod +x scripts/sync_expe_out.sh
while ps -p ${passive_pid} > /dev/null
do
  sleep 600
  scripts/sync_expe_out.sh "${work_directory}/demand-response-expe/out" "${save_directory}"
done