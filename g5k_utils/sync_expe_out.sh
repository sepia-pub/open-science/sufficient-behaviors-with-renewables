#!/bin/bash
#script to sync ref_directory content to mirror_directory
ref_directory=$1
mirror_directory=$2
rsync -zarv "${ref_directory}" "${mirror_directory}"