#!/bin/bash
#launch all the experiment in work_directory and
# copy their result in expe_directory at the end
START_DATE=$(date +%Y-%m-%dT%H:%M:%S)
work_directory=$2
expe_directory=$1
cp -r "$expe_directory" "$work_directory"
cd "$work_directory" || exit
cd "demand-response-expe" || exit
source scripts/install_nix.sh
chmod +x scripts/prepare_workload.sh
nix-shell --pure -A exp_env --run scripts/prepare_workload.sh
chmod +x scripts/run_expe.sh
nix-shell --pure -A exp_env --run scripts/run_expe.sh
mkdir -p "${expe_directory}/../result-experiments"
scripts/sync_expe_out.sh "." "${expe_directory}/../result-experiments"
END_DATE=$(date +%Y-%m-%dT%H:%M:%S)
echo "Experiments started at $START_DATE"
echo "Experiments ended at $END_DATE"