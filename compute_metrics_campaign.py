import pandas as pd
from typing import List

from scripts.util import generate_expe_dict_from_json
from scripts.stat_tools import compute_metrics, compute_metrics_relative
import concurrent.futures
import argparse
import json


class UndefinedColor(Exception):
    pass


def compute_metrics_behavior(xp, behavior: str, seed, window_mode: str, red_windows: List[List[int]],
                             yellow_windows: List[List[int]],
                             begin_time: int, end_time: int,sanity_check: bool, out_dir: str, energy_file:str,
                             offset_production_day,
                             control_exp_metrics):
    current_exp_metrics = compute_metrics(xp, behavior, seed, window_mode, red_windows, yellow_windows,
                                          begin_time, end_time, sanity_check=sanity_check,
                                          out_dir=out_dir,energy_file=energy_file,offset_production_day=offset_production_day)
    current_exp_metrics_dataframe = pd.DataFrame(current_exp_metrics, index=[0])
    current_metrics_relative_dataframe = pd.DataFrame()
    if not(control_exp_metrics is None):
        current_metrics_relative = compute_metrics_relative(control_exp_metrics, current_exp_metrics)
        current_metrics_relative_dataframe = pd.DataFrame(current_metrics_relative, index=[0])
    return current_exp_metrics_dataframe, current_metrics_relative_dataframe


def compute_monolithic_behavior_aux(xp, window_mode: str, red_windows: List[List[int]], yellow_windows: List[List[int]],
                                    begin_time: int, end_time: int, sanity_check: bool, out_dir: str,energy_file :str,
                                    offset_production_day):
    behavior = "replay_user_rigid"
    control_exp_metrics = compute_metrics(xp, behavior, 0, window_mode,
                                          red_windows, yellow_windows, begin_time, end_time, sanity_check,
                                          out_dir, energy_file,offset_production_day=offset_production_day)
    control_exp_metrics_dataframe = pd.DataFrame(control_exp_metrics, index=[0])
    print("Rigid finished, Now monolithic behavior")
    new_metrics = pd.DataFrame()
    new_metrics = pd.concat([new_metrics, control_exp_metrics_dataframe])
    new_metrics_relative = pd.DataFrame()
    for behavior in ["dm_user_multi_behavior_degrad", "dm_user_multi_behavior_reconfig",
                     "dm_user_multi_behavior_renonce"]:
        current_exp_metrics, current_exp_metrics_relative = compute_metrics_behavior(xp, behavior, 0, window_mode,
                                                                                     red_windows, yellow_windows,
                                                                                     begin_time, end_time, sanity_check,
                                                                                     out_dir, energy_file,offset_production_day,
                                                                                     control_exp_metrics
                                                                                     )
        new_metrics = pd.concat([new_metrics, current_exp_metrics], ignore_index=True)
        new_metrics_relative = pd.concat([new_metrics_relative, current_exp_metrics_relative], ignore_index=True)

    return control_exp_metrics, new_metrics, new_metrics_relative


def compute_monolithic_behavior(expe_done_dict, red_windows_dict, yellow_windows_dict,
                                begin_time: int, end_time: int, sanity_check: bool, out_dir: str,energy_file_dict,offset_production_day):
    _,expe_range, window_mode_list, _ = expe_done_dict.range()
    metrics = pd.DataFrame()
    metrics_relative = pd.DataFrame()
    control_exp_metrics_dict = {}
    for xp in expe_range:
        control_exp_metrics_dict[xp] = {}
        for window_mode in window_mode_list:
            # Baseline expe
            red_windows = red_windows_dict[xp][window_mode]
            yellow_windows = yellow_windows_dict[xp][window_mode]
            control_exp_metrics, new_metrics, new_metrics_relative = compute_monolithic_behavior_aux(xp, window_mode,
                                                                                                     red_windows,
                                                                                                     yellow_windows,
                                                                                                     begin_time,
                                                                                                     end_time,
                                                                                                     sanity_check,
                                                                                                     out_dir,
                                                                                                     energy_file_dict[window_mode],
                                                                                                     offset_production_day)
            control_exp_metrics_dict[xp][window_mode] = control_exp_metrics
            metrics = pd.concat([metrics, new_metrics])
            metrics_relative = pd.concat([metrics_relative, new_metrics_relative])
            # Multi_behavior_expe
    return metrics, metrics_relative, control_exp_metrics_dict


def compute_metrics_all_expe_parr(expe_done_dict, begin_time: int, end_time: int, red_windows_dict,
                                  yellow_windows_dict, compute_relative: bool, sanity_check: bool, maximum_worker: int,
                                  out_dir: str,energy_file_dict,offset_production_day):
    control_exp_metrics_dict = {}
    if compute_relative:
        metrics, metrics_relative, control_exp_metrics_dict = compute_monolithic_behavior(expe_done_dict,
                                                                                          red_windows_dict,
                                                                                          yellow_windows_dict,
                                                                                          begin_time,
                                                                                          end_time, sanity_check,
                                                                                          out_dir,energy_file_dict,
                                                                                          offset_production_day)
    else:
        metrics = pd.DataFrame()
        metrics_relative = pd.DataFrame()
    print("Monolithic behavior finished, now MultiBehavior")
    nb_expe_done = 0
    nb_expe_total = len(expe_done_dict.iter())
    progress=0
    with concurrent.futures.ProcessPoolExecutor(max_workers=maximum_worker) as executor:
        instances = []
        for (behavior_name,xp, window_mode, color, seed) in expe_done_dict.iter():
            red_windows = red_windows_dict[xp][window_mode]
            yellow_windows = yellow_windows_dict[xp][window_mode]
            energy_file = energy_file_dict[window_mode]
            control_exp_metrics = None
            if compute_relative:
                control_exp_metrics = control_exp_metrics_dict[xp][window_mode]
            if color == "red":
                behavior = "dm_user_multi_behavior_" + behavior_name
            elif color == "yellow_red":
                behavior = "dm_user_multi_behavior_" + behavior_name + "_yellow"
            instances.append(executor.submit(compute_metrics_behavior, xp, behavior,
                                             seed, window_mode, red_windows, yellow_windows,
                                             begin_time, end_time,sanity_check, out_dir, energy_file,
                                             offset_production_day,
                                             control_exp_metrics))

        for instance in concurrent.futures.as_completed(instances):
            nb_expe_done+=1
            if nb_expe_done*10//nb_expe_total > progress:
                print(f"progress {nb_expe_done}/{nb_expe_total}")
                progress+=1
            current_exp_metrics, current_exp_metrics_relative = instance.result()
            metrics = pd.concat([metrics, current_exp_metrics], ignore_index=True)
            metrics_relative = pd.concat([metrics_relative, current_exp_metrics_relative], ignore_index=True)
    metrics.sort_values(by=["XP", "behavior", "seed"], ignore_index=True, inplace=True)
    if compute_relative:
        metrics_relative.sort_values(by=["XP", "behavior", "seed"], ignore_index=True, inplace=True)
    return metrics, metrics_relative


def compute_metrics_all_expe_seq(expe_done_dict, begin_time: int, end_time: int, red_windows_dict,
                                 yellow_windows_dict, sanity_check, out_dir):
    metrics: pd.DataFrame = pd.DataFrame()
    metrics_relative: pd.DataFrame = pd.DataFrame()

    for xp in expe_done_dict.keys():

        for window_mode in expe_done_dict[xp].keys():
            # Baseline expe
            red_windows = red_windows_dict[xp][window_mode]
            yellow_windows = yellow_windows_dict[xp][window_mode]
            control_exp_metrics, new_metrics, new_metrics_relative = compute_monolithic_behavior_aux(xp, window_mode,
                                                                                                     red_windows,
                                                                                                     yellow_windows,
                                                                                                     begin_time,
                                                                                                     end_time,
                                                                                                     sanity_check,
                                                                                                     out_dir)
            metrics = pd.concat([metrics, new_metrics])
            metrics_relative = pd.concat([metrics_relative, new_metrics_relative])
            # Other expe
            print("Monolithic finished, Now multi behavior")
            behavior_list = ["dm_user_multi_behavior", "dm_user_multi_behavior_yellow"]
            for j in range(len(behavior_list)):
                behavior = behavior_list[j]
                for seed in expe_done_dict[xp][window_mode]:
                    if seed % 10 == 0:
                        print(seed)
                    current_exp_metrics, current_exp_metrics_relative = compute_metrics_behavior(xp, behavior,
                                                                                                 seed + j * 512,
                                                                                                 window_mode,
                                                                                                 red_windows,
                                                                                                 yellow_windows,
                                                                                                 begin_time, end_time,
                                                                                                 sanity_check, out_dir,
                                                                                                 control_exp_metrics)
                    metrics = pd.concat([metrics, current_exp_metrics], ignore_index=True)
                    metrics_relative = pd.concat([metrics_relative, current_exp_metrics_relative], ignore_index=True)

    metrics.sort_values(by=["XP", "behavior", "seed"], ignore_index=True, inplace=True)
    metrics_relative.sort_values(by=["XP", "behavior", "seed"], ignore_index=True, inplace=True)
    return metrics, metrics_relative


def merge_data(expe_done_dict, out_dir):
    """ merge the data produced by compress mode into a single dataframe"""
    result_df = pd.DataFrame()
    for (behavior_name,xp, window_mode, color, seed) in expe_done_dict.iter():
        if color == "red":
            data_xp = pd.read_csv(
                f"{out_dir}/expe{xp}/dm_user_multi_behavior_{behavior_name}_{window_mode}_{seed}/log/condensed_stat.csv")
        elif color == "yellow_red":
            data_xp = pd.read_csv(f"{out_dir}/expe{xp}/dm_user_multi_behavior_{behavior_name}_{window_mode}_yellow_{seed}/log"
                                  f"/condensed_stat.csv")
        else:
            print("Error Undefined Color")
            raise UndefinedColor
        result_df = pd.concat([result_df, data_xp])
    result_df.sort_values(by=["XP", "behavior", "seed"], ignore_index=True, inplace=True)
    return result_df


def generate_dict_from_file(json_filename: str):
    with open(json_filename) as json_file:
        result = json.load(json_file)
        return result


def main():
    argument_parser = argparse.ArgumentParser(
        description="compute diverse metrics on data produce in output directory")
    argument_parser.add_argument("--out-dir", type=str,
                                 help="output directory from where to fetch data", required=True)
    argument_parser.add_argument("--expe-done", type=str,
                                 help="experiment done dictionary save usually a json file (for example : "
                                      "expe_done_dict_0.json)", required=True)
    argument_parser.add_argument("--windows-dict",
                                 help="windows computed dictionary save usually a json file (for example : "
                                      "window_dict_0.json)", required=True)
    argument_parser.add_argument("--begin-day", type=int,
                                 help="The day the experiments began (for example 1 )", required=True)
    argument_parser.add_argument("--end-day", type=int,
                                 help="The day the experiments ended (for example 8)", required=True)
    argument_parser.add_argument("--threads", type=int,
                                 help="number of threads (default : 1 sequential)", default=1, required=False)
    argument_parser.add_argument("--output", type=str, required=True,
                                 help="name of the output csv file (example : out/metrics_campaign_2)")
    argument_parser.add_argument("--merge", action="store_true",
                                 help="merge the already computed data when using compress_mode")
    argument_parser.add_argument("--energy-file", default=None,nargs="*",
                                 help="provide the production file to compute the energy difference "
                                      "between production and consumption ")
    argument_parser.add_argument("--offset-production-day", default=0,type=int,
                                 help="provide the offset date for energy production needed "
                                      "For example, if the provided energy production start at January 1st "
                                      "and the experiment start the 10th you must give 10")
    args = argument_parser.parse_args()
    print(args)
    expe_done_dict = generate_expe_dict_from_json(args.expe_done)
    windows_dict = generate_dict_from_file(args.windows_dict)
    energy_file_dict ={}
    for window_mode,i in   zip(expe_done_dict.window_mode_list,range(len(expe_done_dict.window_mode_list))) :
        if args.energy_file is None :
            energy_file_dict[window_mode] = None
        else :
            energy_file_dict[window_mode] = args.energy_file[i]
    if not args.merge:
         metrics, metrics_relative = compute_metrics_all_expe_parr(expe_done_dict =expe_done_dict,
                                                                      begin_time=args.begin_day * 24 * 3600, end_time= args.end_day*24*3600,
                                                                      red_windows_dict = windows_dict["red_windows"],
                                                                      yellow_windows_dict=windows_dict["yellow_windows"],
                                                                      sanity_check=True, maximum_worker = args.threads,
                                                                      out_dir = args.out_dir, compute_relative =True,
                                                                      energy_file_dict=energy_file_dict,offset_production_day=args.offset_production_day)
         metrics.to_csv(args.output + ".csv")
         metrics_relative.to_csv(args.output + "_relative.csv")
    else:
        metrics = merge_data(expe_done_dict, args.out_dir)
        print(metrics)
        metrics.to_csv(args.output + "merged.csv")


if __name__ == "__main__":
    main()
