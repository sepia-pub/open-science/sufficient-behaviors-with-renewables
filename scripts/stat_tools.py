import json
import matplotlib.patches
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pandas import DataFrame
from random import random
from scripts.util import energy_consumed_in_windows_fast, energy_consumed_in, JobSetMulticore, time_length_windows
import evalys.visu.legacy as vleg
import os
import scipy.integrate


def measure_delay(out_dir):
    delay_file = pd.read_csv(out_dir + "/user_stats_behaviors.csv")
    delay_data = delay_file["time_delayed"]
    return delay_data.sum()


# Execution time analysis
def get_time_list_dict(expe_done_dict, out_dir):
    """ given an ExpeDict compute the list of time they took to execute"""
    time_list = []
    for (behavior_name,xp, window_mode, color, seed) in expe_done_dict.iter():
        if color == "red":
            log_filename = f"{out_dir}/expe{xp}/dm_user_multi_behavior_{behavior_name}{window_mode}_{seed}/log/python-log.log"
        elif color == "yellow_red":
            log_filename = f"{out_dir}/expe{xp}/dm_user_multi_behavior_{behavior_name}{window_mode}_yellow_{seed}/log/python-log.log"
        else:
            print("Unknown color for time_list computation")
            log_filename = None
        with open(log_filename) as logfile:
            lines = logfile.readlines()
            expe_time = int(lines[2])
            time_list.append(expe_time)
    return time_list


def number_of_jobs(workload_dir: str):
    json_filename_list = os.listdir(workload_dir)
    nb_jobs = 0
    for json_filename in json_filename_list:
        json_file = open(workload_dir + "/" + json_filename)
        workload_user = json.load(json_file)
        nb_jobs += len(workload_user["jobs"])
    return nb_jobs


def get_number_of_jobs_dict(expe_done_dict, workload_dir):
    """ given the dictionary of the experiment done compute the list of number of jobs was submitted during these experiment"""
    result = []
    _,expe_range, _, _ = expe_done_dict.range()
    nb_jobs_dict = {}
    for xp in expe_range:
        nb_jobs_dict[xp] = number_of_jobs(f"{workload_dir}/expe{xp}")
    for (behavior_name,xp, _window_mode, _color, _seed) in expe_done_dict.iter():
        result.append(nb_jobs_dict[xp])
    return result


def get_expe_associated(expe_done_dict):
    result = []
    for (_behavior_name,xp, _window_mode, _color, _seed) in expe_done_dict.iter():
        result.append(int(xp))
    return result


def plot_exec_time_aux(nb_jobs, exec_time, expe_associated):
    plt.figure(dpi=1200)
    colors = {}
    for xp in expe_associated:
        colors[xp] = (random(), random(), random())

    new_colors = [colors[xp] for xp in expe_associated]
    print(new_colors)
    fig = plt.figure(dpi=300, figsize=(8, 6))
    ax = plt.gca()
    plt.scatter(nb_jobs, exec_time, c=new_colors)
    legends_element = []
    for xp, color in colors.items():
        legends_element.append(matplotlib.patches.Patch(facecolor=color, edgecolor="black", label=str(xp)))
    ax.legend(handles=legends_element, loc="right",
              title="Expe number", bbox_to_anchor=(1, 0.5))
    ax.grid("on")
    plt.xlabel("Number of jobs")
    plt.ylabel("Execution_time (seconds) ")


def plot_exec_time(expe_done_dict, workload_dir, out_dir, save_filename):
    exec_time = get_time_list_dict(expe_done_dict, out_dir)
    nb_jobs = get_number_of_jobs_dict(expe_done_dict, workload_dir)
    expe_associated = get_expe_associated(expe_done_dict)
    plot_exec_time_aux(nb_jobs, exec_time, expe_associated)
    plt.savefig(save_filename, bbox_inches="tight")
    plt.close()


def plot_queue_load(expe_done_dict, out_dir):
    for (behavior_name,xp_num, window_mode, color, seed) in expe_done_dict.iter():
        if color == "red":
            expe_dir = f"{out_dir}/expe{xp_num}/dm_user_multi_behavior_{behavior_name}{window_mode}_{seed}"
        elif color == "yellow_red":
            expe_dir = f"{out_dir}/expe{xp_num}/dm_user_multi_behavior_{behavior_name}{window_mode}_yellow_{seed}"
        js = JobSetMulticore.from_csv(expe_dir + "/_jobs.csv")
        fig, axe = plt.subplots(nrows=2, sharex=True, figsize=(16, 8), tight_layout=True)
        fig.suptitle(expe_dir, fontsize=16)
        vleg.plot_load(js.utilisation, js.MaxProcs, time_scale=False, legend_label="utilisation", ax=axe[0])
        vleg.plot_load(js.queue, js.MaxProcs, time_scale=False, legend_label="queue size", ax=axe[1])
        plt.savefig(f"{expe_dir}/log/plot_load_queue.svg")
        plt.close(fig)


def plot_job_distrib(workload_dir: str):
    json_filename_list = os.listdir(workload_dir)
    jobs_subtime_list = []
    for json_filename in json_filename_list:
        json_file = open(workload_dir + "/" + json_filename)
        workload_user = json.load(json_file)
        job_list = workload_user["jobs"]
        for job in job_list:
            jobs_subtime_list.append(job["subtime"])
    jobs_subtime_list.sort()
    subtime_dict = {}
    counter = 0
    for subtime in jobs_subtime_list:
        counter += 1
        subtime_dict[subtime] = counter
    X = list(subtime_dict.keys())
    Y = list(subtime_dict.values())
    line = plt.plot(X, Y, label="cumulated number of job submitted over time")
    plt.xlabel("Time")
    plt.ylabel("Number of jobs")
    plt.legend()
    plt.title(workload_dir)
    print(subtime_dict)


def final_behaviors(behaviors_object):
    return final_behaviors_list(behaviors_object.values)


def final_behaviors_list(behaviors_list):
    for element in behaviors_list:
        if element in ["rigid", "degrad", "renonce", "reconfig"]:
            return element
    return "unknown_behavior"


def merge_with_behaviors(data: pd.DataFrame, expe_dir):
    """ add behaviors_user_stats info to job dataframe"""
    behavior_file: pd.DataFrame = pd.read_csv(expe_dir + "/user_stats_behaviors.csv")
    orig_subtime = behavior_file[["job_id", "user", "submission_time"]]
    orig_subtime = orig_subtime.groupby(["job_id", "user"]).min()
    orig_subtime = orig_subtime.rename({"submission_time": "original_submission_time"}, axis="columns")
    new_data = pd.merge(data, orig_subtime, on=["job_id", "user"])
    delay_time = behavior_file[["job_id", "user", "time_delayed"]]
    delay_time = delay_time.groupby(["job_id", "user"]).sum(numeric_only=True)
    new_data = pd.merge(new_data, delay_time, on=["job_id", "user"])
    final_behavior = behavior_file[["job_id", "user", "behavior_name"]]
    final_behavior = final_behavior.groupby(["job_id", "user"]).agg({"behavior_name": final_behaviors})
    final_behavior = final_behavior.rename({"behavior_name": "final_behavior"}, axis="columns")
    new_data = pd.merge(new_data, final_behavior, on=["job_id", "user"])
    return new_data


def correction_waiting_time(data_behaviors_jobs: pd.DataFrame, sanity_check: bool):
    new_data = data_behaviors_jobs
    new_data["is_reconfig"] = new_data["final_behavior"] == "reconfig"
    new_data["is_reconfig"] = new_data["is_reconfig"].astype(int)
    new_data["corrected_execution_time"] = new_data["execution_time"] / (1 + new_data["is_reconfig"])
    new_data["corrected_slowdown_1"] = ((new_data["stretch"] * new_data["execution_time"] + new_data["time_delayed"])
                                        / new_data["corrected_execution_time"])
    new_data["corrected_waiting_time_1"] = new_data["waiting_time"] + new_data["time_delayed"]
    if sanity_check:
        new_data["corrected_slowdown_2"] = ((new_data["finish_time"] - new_data["original_submission_time"])
                                            / new_data["corrected_execution_time"])
        new_data["corrected_waiting_time_2"] = (new_data["starting_time"] - new_data["original_submission_time"])
    new_data = new_data.drop(["is_reconfig"], axis="columns")
    return new_data


def true_rigid_job_fun(x):
    true_rigid_simple = len(x.values) == 1 and x.values[0] == "rigid"
    true_rigid_consider = len(x.values) == 2 and ("consider" == x.values[0][:8]) and (x.values[1] == "rigid")
    return true_rigid_consider or true_rigid_simple


def compute_power_consumed(data: pd.DataFrame, start_date):
    """ using energy data provided by the simulation
    computed the power use by the machine over time"""
    new_data = data.drop(["event_type", "wattmin", "epower"], axis=1)
    new_data = new_data.drop_duplicates(subset="time")
    power_data = new_data.diff(1)
    power_data = power_data.rename({"energy": "diff_energy"})
    power_data["power"] = power_data["diff_energy"] / power_data["time"]
    power_data["energy"] = new_data["energy"]
    power_data["power"][0] = 0
    power_data["energy"][0] = 0
    power_data["time"] = new_data["time"]
    power_data["datetime"] = pd.to_datetime(new_data["time"] + start_date, unit="s")
    return power_data


def compute_power_produced(data: pd.DataFrame, start_date, start_time, end_time):
    power_data = pd.DataFrame()
    power_data["time"] = data["time"]
    power_data["power"] = data["power"]
    power_data = power_data[start_time <= power_data.time]
    power_data = power_data[power_data.time <= end_time]
    power_data["datetime"] = pd.to_datetime(data["time"] + start_date, unit="s")
    power_data["time"] = data["time"]
    power_data["time"] = power_data["time"] - power_data["time"].min()
    return power_data


def produced_energy_total(produced_energy_data, start_time, end_time):
    """ Return  the produced energy total given produced_energy_data between start_time and end_time we assume that
    an entry of produced_energy_data is given for time start_time and end_time"""
    selected_produced_energy_data = produced_energy_data[produced_energy_data.time >= start_time]
    selected_produced_energy_data = selected_produced_energy_data[selected_produced_energy_data.time <= end_time]
    return scipy.integrate.simpson(selected_produced_energy_data["power"], x=selected_produced_energy_data["time"])


def load_consumed_energy(consumed_energy_path):
    energy_data = pd.read_csv(consumed_energy_path)
    energy_data = energy_data.groupby("time").max()
    energy_data.reset_index(inplace=True)
    energy_data = energy_data.rename(columns={"epower": "power"})
    return energy_data


def compute_energy_delta_slow(produced_energy_data: pd.DataFrame, consumed_energy_data: pd.DataFrame, start_time,
                              end_time):
    new_consumed_energy_data = consumed_energy_data[consumed_energy_data.time <= end_time]
    new_consumed_energy_data = new_consumed_energy_data[new_consumed_energy_data.time >= start_time]
    new_consumed_energy_data = new_consumed_energy_data.rename(
        columns={"power": "power_consumed"})
    new_produced_energy_data = produced_energy_data[produced_energy_data.time <= end_time]
    new_produced_energy_data = new_produced_energy_data[new_produced_energy_data.time >= start_time]
    new_produced_energy_data = new_produced_energy_data.rename(
        columns={"power": "power_produced"})
    energy_total_data = pd.DataFrame(columns=["time", "power_consumed", "power_produced"])
    first_produced_power_index = produced_energy_data[produced_energy_data.time <= start_time]["time"].idxmax()
    power_produced = produced_energy_data["power"][first_produced_power_index]
    first_consumed_power_index = consumed_energy_data[consumed_energy_data.time <= start_time]["time"].idxmax()
    power_consumed = consumed_energy_data["power"][first_consumed_power_index]
    index_consumed = first_consumed_power_index + 1
    last_consumed_index = new_consumed_energy_data.index.max()
    index_produced = first_produced_power_index + 1
    last_produced_index = new_produced_energy_data.index.max()
    for i in range(end_time - start_time):
        time_to_fill = start_time + i
        if index_consumed <= last_consumed_index and new_consumed_energy_data["time"][index_consumed] <= time_to_fill:
            power_consumed = new_consumed_energy_data["power_consumed"][index_consumed]
            index_consumed += 1
        if index_produced <= last_produced_index and new_produced_energy_data["time"][index_produced] <= time_to_fill:
            power_produced = new_produced_energy_data["power_produced"][index_produced]
            index_produced += 1
        energy_total_data.loc[i, :] = [time_to_fill, power_consumed, power_produced]
    return energy_total_data

def compute_energy_delta(produced_energy_data,consumed_energy_data,start_time,end_time,offset_production_day) :
    """ Compute the positive and negative difference between power produced
    and power consumed express the balance in Joules"""
    energy_dataframe = compute_energy_delta_dataframe(produced_energy_data, consumed_energy_data, start_time, end_time, offset_production_day)
    positive_energy_balance = energy_dataframe[energy_dataframe.energy_delta_time > 0]["energy_delta_time"].sum()
    negative_energy_balance = - energy_dataframe[energy_dataframe.energy_delta_time < 0]["energy_delta_time"].sum()
    return positive_energy_balance,negative_energy_balance

def compute_energy_delta_dataframe(produced_energy_data: pd.DataFrame, consumed_energy_data: pd.DataFrame, start_time,
                                   end_time,offset_production_day):
    new_consumed_energy_data = consumed_energy_data[consumed_energy_data.time <= end_time]
    new_consumed_energy_data = new_consumed_energy_data[new_consumed_energy_data.time >= start_time]
    new_consumed_energy_data = new_consumed_energy_data.rename(
        columns={"power": "power_consumed", "energy": "energy_consumed"})
    new_produced_energy_data = produced_energy_data[produced_energy_data.time <= end_time + offset_production_day*24*3600]
    new_produced_energy_data = new_produced_energy_data[new_produced_energy_data.time >= start_time + offset_production_day*24*3600]
    new_produced_energy_data = new_produced_energy_data.rename(
        columns={"power": "power_produced", "energy": "energy_produced"})
    new_produced_energy_data["time"] -= offset_production_day*24*3600
    new_energy_dataframe : pd.DataFrame = pd.concat([new_consumed_energy_data, new_produced_energy_data])
    first_consumed_power_index = consumed_energy_data[consumed_energy_data.time <= start_time]["time"].idxmax()
    first_power_consumed = consumed_energy_data["power"][first_consumed_power_index]
    first_produced_power_index = produced_energy_data[produced_energy_data.time <= start_time]["time"].idxmax()
    first_power_produced = produced_energy_data["power"][first_produced_power_index]
    new_energy_dataframe.sort_values(["time"], inplace=True, ignore_index=True)
    new_energy_dataframe.at[0,"power_produced"] = first_power_produced
    new_energy_dataframe.at[0,"power_consumed"] = first_power_consumed
    new_energy_dataframe = new_energy_dataframe.set_index("time")
    new_energy_dataframe["power_produced"].interpolate(method="index", inplace=True)
    new_energy_dataframe["power_consumed"].interpolate(method="pad", inplace=True)
    new_energy_dataframe.reset_index(inplace=True)

    # look for case where the curve of power consumed and power produced cross

    curve_cross = (new_energy_dataframe["power_produced"].shift(-1) > new_energy_dataframe["power_consumed"]) & \
                  (new_energy_dataframe["power_produced"] < new_energy_dataframe["power_consumed"])
    curve_cross_2 = (new_energy_dataframe["power_produced"].shift(-1) < new_energy_dataframe["power_consumed"]) & \
                  (new_energy_dataframe["power_produced"] > new_energy_dataframe["power_consumed"])
    curve_cross_all = curve_cross | curve_cross_2
    part_to_split = new_energy_dataframe[curve_cross_all]
    start_time = part_to_split["time"]
    end_time = new_energy_dataframe["time"][part_to_split.index+1]
    end_time.index -= 1
    start_power_produced = part_to_split["power_produced"]
    power_consumed = part_to_split["power_consumed"] - start_power_produced
    end_power_produced = new_energy_dataframe["power_produced"][part_to_split.index+1]
    end_power_produced.index -= 1
    end_power_produced = end_power_produced - start_power_produced
    # we interpolate the crossing time value
    new_time_split = start_time + (power_consumed/end_power_produced) * ( end_time - start_time)
    new_time_split_dataframe = pd.DataFrame(data=new_time_split,columns=["time"])
    new_energy_dataframe = pd.concat([new_energy_dataframe,new_time_split_dataframe])
    # we interpolate in the crossing time value to get power_consumed and produced
    new_energy_dataframe.sort_values(["time"], inplace=True, ignore_index=True)
    new_energy_dataframe = new_energy_dataframe.set_index("time")
    new_energy_dataframe["power_produced"].interpolate(method="index", inplace=True)
    new_energy_dataframe["power_consumed"].interpolate(method="pad", inplace=True)
    new_energy_dataframe.reset_index(inplace=True)

    new_energy_dataframe["delta_time"] = new_energy_dataframe["time"].shift(-1) - new_energy_dataframe["time"]
    new_energy_dataframe["energy_produced_time"] = new_energy_dataframe["delta_time"] * (new_energy_dataframe["power_produced"] + new_energy_dataframe["power_produced"].shift(-1)) / 2
    new_energy_dataframe["energy_consumed_time"] = new_energy_dataframe["power_consumed"] * new_energy_dataframe["delta_time"]
    new_energy_dataframe["energy_delta_time"] = new_energy_dataframe["energy_produced_time"] - new_energy_dataframe["energy_consumed_time"]
    return new_energy_dataframe

def user_behavior_related_stat(data, expe_dir, sanity_check, out):
    """ Use user_behavior loggging to complete the default jobs data"""
    dm_stat_path = f"{expe_dir}/user_stats.csv"
    behavior_job_data = None
    if os.path.exists(f"{expe_dir}/user_stats.csv"):
        # print("correcting data")
        dm_stat = pd.read_csv(dm_stat_path, dtype={'nb_jobs': "int", "core_seconds": "int"})
        out["user_stat_renonced_jobs"] = dm_stat["nb_jobs"][1]
        out["user_stat_delayed_jobs"] = dm_stat["nb_jobs"][2]
        out["user_stat_degraded_jobs"] = dm_stat["nb_jobs"][3]
        out["user_stat_reconfig_jobs"] = dm_stat["nb_jobs"][4]
        out["user_stat_rigid_jobs"] = dm_stat["nb_jobs"][0]
        if out["user_stat_rigid_jobs"] == 1:
            out["user_stat_rigid_jobs"] = out["#jobs"]
        out["user_stat_consider_degrad"] = dm_stat["nb_jobs"][5]
        out["user_stat_consider_reconfig"] = dm_stat["nb_jobs"][6]
    if os.path.exists(f"{expe_dir}/user_stats_behaviors.csv"):
        data_2: DataFrame = pd.read_csv(f"{expe_dir}/user_stats_behaviors.csv", dtype={'profile': 'str'})
        if not data_2.empty:
            behavior_job_data = merge_with_behaviors(data, expe_dir)
            # print(data_2)
            behaviors_rigid = data_2.groupby(["user", "job_id"]).agg({'behavior_name': true_rigid_job_fun})
            behaviors_rigid = behaviors_rigid[behaviors_rigid.behavior_name]
            out["true_rigid_jobs"] = len(behaviors_rigid)
            out["mean_delay"] = behavior_job_data["time_delayed"].sum() / out["#jobs"]
            out["max_delay"] = behavior_job_data["time_delayed"].max()
            behavior_stat_count = data_2.groupby(["behavior_name"]).count()["job_id"]
            for behavior in ["renonce", "reconfig", "degrad", "rigid", "consider_degrad", "consider_reconfig"]:
                try:
                    out[f"{behavior}_jobs"] = behavior_stat_count[behavior]
                except KeyError:
                    out[f"{behavior}_jobs"] = 0

            try:
                out["number_of_see_you_later"] = behavior_stat_count["C_you_later"]
            except KeyError:
                out["number_of_see_you_later"] = 0
            behavior_stat_see_you_jobs = data_2.groupby(["user", "job_id"]).nunique()
            out["C_you_later_jobs"] = len(behavior_stat_see_you_jobs[
                                              behavior_stat_see_you_jobs.submission_time > 1])
            if sanity_check:
                out["C_you_later_jobs_2"] = len(behavior_stat_see_you_jobs[behavior_stat_see_you_jobs.time_delayed > 1])

            # Corrected waiting time and slowdown: cross data with user_behavior_stat
            corrected_data = correction_waiting_time(behavior_job_data, sanity_check)
            out["mean_corrected_wtime"] = corrected_data["corrected_waiting_time_1"].mean()
            out["max_corrected_wtime"] = corrected_data["corrected_waiting_time_1"].max()
            out["mean_corrected_sdown"] = corrected_data["corrected_slowdown_1"].mean()
            out["max_corrected_sdown"] = corrected_data["corrected_slowdown_1"].max()

            if sanity_check:
                out["mean_corrected_wtime_2"] = corrected_data["corrected_waiting_time_2"].mean()
                out["max_corrected_wtime_2"] = corrected_data["corrected_waiting_time_2"].max()
                out["mean_corrected_sdown_2"] = corrected_data["corrected_slowdown_2"].mean()
                out["max_corrected_sdown_2"] = corrected_data["corrected_slowdown_2"].max()
                check_correction_wtime = abs(corrected_data["corrected_waiting_time_1"] -
                                             corrected_data["corrected_waiting_time_2"])
                max_diff_wtime = check_correction_wtime.max()
                out["max_diff_wtime_sanity"] = max_diff_wtime
                check_correction_stime = abs(corrected_data["corrected_slowdown_1"] -
                                             corrected_data["corrected_slowdown_2"])
                max_diff_sdown = check_correction_stime.max()
                out["max_diff_sdown_sanity"] = max_diff_sdown
    return behavior_job_data


def compute_metrics(exp_num, behavior, seed, window_type, red_windows, yellow_windows, begin_time, end_time,
                    sanity_check, out_dir,energy_file,offset_production_day):
    """Returns a dictionnary of metrics for the given expe."""
    # print(behavior)
    behavior_name = behavior
    if "yellow" in behavior:
        behavior_name = behavior[:-7]
        window_type += "_yellow"
    expe_dir = f"{out_dir}/expe{exp_num}/{behavior_name}{window_type}_{seed}"
    # print(expe_dir)
    # Observed window for the scheduling metrics:
    data: DataFrame = pd.read_csv(f"{expe_dir}/_jobs.csv", dtype={'profile': 'str'})
    data = data.rename({"workload_name": "user"}, axis="columns")
    out = {"XP": exp_num, "dir": expe_dir, "behavior": behavior, "#jobs": len(data.index), "seed": seed}
    energy_array_red = energy_consumed_in_windows_fast(red_windows, expe_dir)
    energy_array_yellow = energy_consumed_in_windows_fast(yellow_windows, expe_dir)
    out["Duration_red (seconds)"] = time_length_windows(red_windows)
    out["Duration_yellow (seconds)"] = time_length_windows(yellow_windows)
    out["Duration_total (seconds)"] = time_length_windows([[begin_time,end_time]])
    out['NRJ_red (Joules)'] = np.sum(energy_array_red)
    out['NRJ_yellow (Joules)'] = np.sum(energy_array_yellow)
    out["NRJ_total (Joules)"] = energy_consumed_in([begin_time, end_time], expe_dir)
    out["NRJ_red_normalized_window_length (Watts)"] = out["NRJ_red (Joules)"] / out["Duration_red (seconds)"]
    out["NRJ_yellow_normalized_window_length (Watts)"] = out["NRJ_yellow (Joules)"] / out["Duration_yellow (seconds)"]
    out["NRJ_total_normalized_window_length (Watts)"] = out["NRJ_total (Joules)"] / out["Duration_total (seconds)"]
    out["mean_waiting_time (seconds)"] = data["waiting_time"].mean()
    out["max_waiting_time (seconds)"] = data["waiting_time"].max()
    out["mean_slowdown"] = data["stretch"].mean()
    out["max_slowdown"] = data["stretch"].max()
    out["window_type"] = window_type
    if window_type == "":
        out["window_type"] = "basic"
    user_behavior_related_stat(data, expe_dir, sanity_check, out)
    if not(energy_file is None ) :
        compute_energy_delta_to_out(energy_file,begin_time,end_time,sanity_check,expe_dir,offset_production_day, out)
    return out


def compute_energy_delta_to_out(energy_file, begin_time, end_time, sanity_check, expe_dir, offset_production_day, out) :
    produced_energy_data = pd.read_csv(energy_file)
    consumed_energy_data = load_consumed_energy(f"{expe_dir}/_consumed_energy.csv")
    positive_energy,negative_energy = compute_energy_delta(produced_energy_data,consumed_energy_data,begin_time,end_time,offset_production_day)
    out["energy overproduced (Joules)"] = positive_energy
    out["energy underproduced (Joules)"] = negative_energy
    out["energy balance 1 (Joules)"] = positive_energy - negative_energy
    if sanity_check :
        out["energy balance 2 (Joules)"] = produced_energy_total(produced_energy_data,begin_time + offset_production_day*3600*24,
                                                                 end_time+ offset_production_day*3600*24) - \
                                           energy_consumed_in([begin_time,end_time],expe_dir)
        out["energy balance relative accuracy"] = (abs(out["energy balance 1 (Joules)"] - out["energy balance 2 ("
                                                                                              "Joules)"])/
                                                     min(out["energy balance 2 (Joules)"],out["energy balance 1 ("
                                                                                              "Joules)"]))
def pct_change(a, b):
    """a + pct_change(a, b) * a = b"""
    return (b / a - 1) * 100


def compute_metrics_relative(control, current):
    """Returns the metrics of current, relative to control.
    Param are dictionnaries for a single expe."""
    res = {"window_type": current["window_type"], "seed": current["seed"], 'XP': current['XP'],
           'NRJ_red (Joules)': current['NRJ_red (Joules)'], 'behavior': current['behavior']}
    for metric in ["#jobs", 'NRJ_red (Joules)', 'NRJ_yellow (Joules)', "NRJ_total (Joules)", "max_slowdown", "max_waiting_time (seconds)", "mean_slowdown",
                   "mean_waiting_time (seconds)"]:
        res[metric] = pct_change(control[metric], current[metric])
    for metric in ["rigid_jobs", "renonce_jobs","C_you_later_jobs","number_of_see_you_later", "degrad_jobs", "reconfig_jobs",
                   "consider_degrad_jobs", "consider_reconfig_jobs"]:
        res[metric] = current[metric] / control["#jobs"] * 100
    # for metric in ["rigid_jobs","renonced_jobs","delayed_jobs","degraded_jobs","reconfig_jobs"] :
    #    res[metric+"_jobspart"] = current[metric]/current["#jobs"]*100
    res["NRJ_red + NRJ_yellow (Joules)"] = pct_change(control["NRJ_red (Joules)"] + control["NRJ_yellow (Joules)"],
                                             current["NRJ_red (Joules)"] + current["NRJ_yellow (Joules)"])
    res["max_delay"] = current["max_delay"]
    res["mean_delay"] = current["mean_delay"]
    res["true_rigid_jobs"] = pct_change(control["#jobs"], current["true_rigid_jobs"])
    res["mean_corrected_sdown_1"] = pct_change(control["mean_slowdown"], current["mean_corrected_sdown"])
    res["mean_corrected_wtime_1"] = pct_change(control["mean_waiting_time (seconds)"], current["mean_corrected_wtime"])
    res["energy overproduced (Joules)"] = pct_change(control["energy overproduced (Joules)"],current["energy overproduced (Joules)"])
    res["energy underproduced (Joules)"] = pct_change(control["energy underproduced (Joules)"],
                                                     current["energy underproduced (Joules)"])
    return res


def reset_id(job_list):
    for job in job_list:
        job["id"] = 0
        job["profile"] = 0


def compare_jobs_dir(workload_dir1, workload_dir2):
    json_filename_list_1 = os.listdir(workload_dir1)
    json_filename_list_1.sort()
    success = True
    for json_filename in json_filename_list_1:
        json_file1 = open(workload_dir1 + "/" + json_filename)
        json_file2 = open(workload_dir2 + "/" + json_filename)
        workload_user1 = json.load(json_file1)
        workload_user2 = json.load(json_file2)
        reset_id(workload_user2["jobs"])
        reset_id(workload_user1["jobs"])
        if workload_user2["jobs"] != workload_user1["jobs"]:
            print(json_filename)
            success = False
        else:
            print(f"same {json_filename}")
    return success
