#!/bin/bash
# launch of the full experimental campaign:
# 164 days with 30 replicates and all effort scenarii
python3 campaign.py --nb-replicat 30 --expe-range -1 --window-mode 8 --nb-days 164 \
--json-behavior behavior_file/big_effort.json behavior_file/low_effort.json behavior_file/max_effort.json behavior_file/medium_effort.json \
--compress-mode --production-file data_energy/energy_trace_sizing_solar.csv data_energy/energy_trace_sizing_solar.csv