#!/bin/bash
#cut the workload for our experiments
curl -L -o - http://www.cs.huji.ac.il/labs/parallel/workload/l_metacentrum2/METACENTRUM-2013-3.swf.gz -k | gunzip > workload/METACENTRUM-2013-3.swf
jun1=44578794;
nov30=60393593;
nb_hour=15;
echo "submit_time >= $jun1 and submit_time <= $nov30, run_time max : $nb_hour hour";
swfFilter workload/METACENTRUM-2013-3.swf \
    -o workload/METACENTRUM_6months.swf \
    --keep_only="submit_time >= ${jun1} and submit_time <= ${nov30}" \
    --partitions_to_select  7 9 11 14 15 18 19;
swfFilter workload/METACENTRUM_6months.swf \
    -o workload/MC_selection_article.swf \
    --keep_only="nb_res <= 18 and run_time <= ${nb_hour}*3600";
