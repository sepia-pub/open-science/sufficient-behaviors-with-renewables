#!/bin/bash
#script to compress the out directory to a file
to_compress=$1
output=$2
tar -I "zstd -3 -T0" -cvf "$output" "$to_compress"