#!/bin/bash
#Compute the metrics of experiments in out_dir
out_dir="out/"
python3 compute_metrics_campaign.py --out-dir "${out_dir}/" \
--expe-done "${out_dir}/"expe_done_dict_0.json \
--windows-dict "${out_dir}"/windows_dict_0.json \
--output campaign_metrics --begin-day 1 --end-day 163 \
--energy-file data_energy/energy_trace_sizing_solar.csv \
--offset-production-day 150 \
--threads 4