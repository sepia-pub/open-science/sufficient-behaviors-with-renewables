import pandas as pd
import json


def save_dict_to_json(dictionary: dict, json_filename: str):
    with open(json_filename, "w+") as dict_save_file:
        json.dump(dictionary, dict_save_file,indent=2)


def convert_odre_data(production_file: str, time_step: int, energy_mix=None):
    """ convert an open data réseau electrique file
    to a simple dataframe with time in seconds and power entry"""
    data_prod = pd.read_csv(production_file, sep=";")
    data_prod.sort_values(["Date", "Heure"], inplace=True, ignore_index=True)
    if energy_mix is None:
        energy_mix = ["Eolien (MW)", "Solaire (MW)"]
    data_prod["power"] = data_prod[energy_mix[0]]
    for energy in energy_mix[1:]:
        data_prod["power"] += data_prod[energy]
    data_prod["time"] = time_step * data_prod.index
    return data_prod[["time", "power"]]


def generate_dict_proba_entry(param_dict, behavior_list, rigid_prob, prefix, suffix):
    remaining_prob = 1 - rigid_prob
    for behavior in behavior_list:
        param_dict[prefix + behavior + suffix] = remaining_prob * 1 / len(behavior_list)
    rigid_prob_key = prefix + "rigid" + suffix
    if rigid_prob_key in param_dict:
        param_dict[rigid_prob_key] += rigid_prob
    else:
        param_dict[rigid_prob_key] = rigid_prob

class WrongProbabilityException(Exception) :
    pass

def generate_behavior_dict(rigid_prob):
    """ Given rigid_prob between 1.0 and 0.0 generate the probability distribution"""
    if rigid_prob >1.0 or rigid_prob < 0.0 :
        raise WrongProbabilityException
    param_dict = {}
    remaining_prob = 1.0 - rigid_prob

    # red behavior multi_core
    possible_behavior_red_multi = ["reconfig", "degrad", "see_you_later", "renonce"]
    generate_dict_proba_entry(param_dict, possible_behavior_red_multi, rigid_prob, "red_prob_", "_multi_core")

    # yellow behavior multi_core
    possible_behavior_yellow_multi = ["degrad", "reconfig", "rigid"]
    generate_dict_proba_entry(param_dict, possible_behavior_yellow_multi, rigid_prob, "yellow_prob_", "_multi_core")

    # red behavior mono core
    possible_behavior_red_mono = ["degrad", "see_you_later", "renonce"]
    generate_dict_proba_entry(param_dict, possible_behavior_red_mono, rigid_prob, "red_prob_", "_mono_core")

    # yellow behavior mono core
    possible_behavior_yellow_mono = ["degrad", "rigid"]
    generate_dict_proba_entry(param_dict, possible_behavior_yellow_mono, rigid_prob, "yellow_prob_", "_mono_core")
    return param_dict


def generate_behavior_json(rigid_prob, json_filename):
    param_dict = generate_behavior_dict(rigid_prob)
    save_dict_to_json(param_dict, json_filename)
