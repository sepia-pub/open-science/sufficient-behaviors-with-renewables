#!/bin/bash
# script to generate a flamegraph from the perf.data given in first argument
# and save to svg format in the file of name the second argument
data=$1
output=$2
perf script -i "$data" > out.perf
stackcollapse-perf.pl --inline out.perf > out.folded
flamegraph.pl out.folded > "$output"
rm out.perf
rm out.folded