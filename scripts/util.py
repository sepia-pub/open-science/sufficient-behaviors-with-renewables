#!/usr/bin/env python3
import os
import os.path
import subprocess
import numpy as np
import pandas
from evalys.jobset import JobSet
from evalys.metrics import compute_load
import json

from typing import List, Tuple

############
# USEFUL VAR
############
ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
WL_DIR = f'{ROOT_DIR}/workload'


############
# TO RUN EXPES WITH ROBIN
############
class RobinInstance(object):
    def __init__(self, output_dir, batcmd, schedcmd, simulation_timeout, ready_timeout, success_timeout,
                 failure_timeout):
        self.output_dir = output_dir
        self.batcmd = batcmd
        self.schedcmd = schedcmd
        self.simulation_timeout = simulation_timeout
        self.ready_timeout = ready_timeout
        self.success_timeout = success_timeout
        self.failure_timeout = failure_timeout

    def to_yaml(self):
        # Manual dump to avoid dependencies
        return f'''output-dir: '{self.output_dir}'
batcmd: "{self.batcmd}"
schedcmd: "{self.schedcmd}"
simulation-timeout: {self.simulation_timeout}
ready-timeout: {self.ready_timeout}
success-timeout: {self.success_timeout}
failure-timeout: {self.failure_timeout}
'''

    def to_file(self, filename):
        create_dir_rec_if_needed(os.path.dirname(filename))
        write_file(filename, self.to_yaml())


class ExpeDict:
    def __init__(self, behavior_name_list, expe_range, window_mode_list, colors_list):
        self.data = {}
        self.behavior_name_list = behavior_name_list
        self.expe_range = expe_range
        self.window_mode_list = window_mode_list
        self.colors_list = colors_list
        for behavior_name in behavior_name_list :
            self.data[behavior_name] = {}
            dict_to_modify_behavior_name = self.data[behavior_name]
            for xp in expe_range:
                dict_to_modify_behavior_name[xp] = {}
                dict_to_modify_xp = dict_to_modify_behavior_name[xp]
                for window_mode in window_mode_list:
                    dict_to_modify_xp[window_mode] = {}
                    dict_to_modify_window_mode = dict_to_modify_xp[window_mode]
                    for color in colors_list:
                        dict_to_modify_window_mode[color] = []
        self.elements = []
        self.data_modified = False

    def add_expe_unsafe(self, behavior_name, expe_num, window_mode, color, seed):
        self.data_modified = True
        self.data[behavior_name][expe_num][window_mode][color].append(seed)

    def add_expe(self, behavior_name, expe_num, window_mode, color, seed):
        if behavior_name in self.behavior_name_list :
            if expe_num in self.expe_range:
                if window_mode in self.window_mode_list:
                    if color in self.colors_list:
                        self.add_expe_unsafe(behavior_name,expe_num, window_mode, color, seed)
                        return
                    else:
                        print("Invalid color to add to the dict")
                else:
                    print("Invalid window_mode to add to the dict")
            else:
                print("Invalid experiment number to add to the dict")
        else:
            print("Invalid behavior name to add to the dict")

    def iter(self) -> List[Tuple[any,any,any,any,any]]:
        if not self.data_modified:
            return self.elements
        self.elements = []
        for behavior_name in self.behavior_name_list :
            for xp in self.expe_range:
                for window_mode in self.window_mode_list:
                    for color in self.colors_list:
                        self.elements += [(behavior_name,xp, window_mode, color, seed) for seed in
                                          self.data[behavior_name][xp][window_mode][color]]
        self.data_modified = False
        return self.elements

    def iter_subpart(self, behavior_name_list_sub=None, expe_range_sub=None, window_mode_list_sub=None, colors_list_sub=None):
        result = []
        if behavior_name_list_sub is None :
            behavior_name_list_sub = self.behavior_name_list
        if expe_range_sub is None:
            expe_range_sub = self.expe_range
        if window_mode_list_sub is None:
            window_mode_list_sub = self.window_mode_list
        if colors_list_sub is None:
            colors_list_sub = self.colors_list
        for behavior_name in behavior_name_list_sub :
            for xp in expe_range_sub:
                for window_mode in window_mode_list_sub:
                    for color in colors_list_sub:
                        result += [(xp, window_mode, color, seed) for seed in self.data[behavior_name][xp][window_mode][color]]
        return result

    def from_json(self, json_filename):
        with open(json_filename, "r") as dict_save_file:
            self.data = json.load(dict_save_file)
        self.behavior_name_list = self.data.keys()
        behavior_example =next(self.behavior_name_list.__iter__())
        dict_behavior_name = self.data[behavior_example]
        self.expe_range = dict_behavior_name.keys()
        xp_example = next(self.expe_range.__iter__())
        dict_expe = dict_behavior_name[xp_example]
        self.window_mode_list = dict_expe.keys()
        window_mode_example = next(self.window_mode_list.__iter__())
        window_mode_dict = dict_expe[window_mode_example]
        self.colors_list = window_mode_dict.keys()
        self.data_modified = True

    def range(self):
        return self.behavior_name_list,self.expe_range, self.window_mode_list, self.colors_list

    def to_json(self, json_filename):
        with open(json_filename, "w+") as dict_save_file:
            json.dump(self.data, dict_save_file)


def generate_expe_dict_from_json(json_filename):
    expe_dict = ExpeDict([], [], [],[])
    expe_dict.from_json(json_filename)
    return expe_dict

def generate_dict_from_json(json_filename) :
    with open(json_filename, "r") as dict_save_file:
        return json.load(dict_save_file)
def gen_batsim_cmd(platform, workload, output_dir, more_flags):
    return f"batsim -p '{platform}' -w '{workload}' -e '{output_dir}/' {more_flags}"


def write_file(filename, content):
    file = open(filename, "w")
    file.write(content)
    file.close()


def create_dir_rec_if_needed(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)


def run_robin(filename):
    return subprocess.run(['robin', filename])


############
# FOR DATA VIZ
############
class JobSetMulticore(JobSet):
    """Custom version of jobset to change the way 'utilisation' is computed."""

    def __init__(self, df, resource_bounds=None, float_precision=6, nb_cores=18):
        JobSet.__init__(self, df, resource_bounds, float_precision)
        self.MaxProcs = len(self.res_bounds) * nb_cores

    @property  # override
    def utilisation(self):
        if self._utilisation is not None:
            return self._utilisation
        self._utilisation = compute_load(self.df, col_begin='starting_time', col_end='finish_time',
                                         col_cumsum='requested_number_of_resources')  # original: proc_alloc
        return self._utilisation




def intersect_windows(window1,window2) :
    if window1[0] <= window2[0] <= window1[1] :
        return True
    if window1[0] <= window2[1] <= window1[1]:
        return True
    if window2[0] <= window1[0] <= window1[1] <= window2[1] :
        return True
    return False


def energy_consumed_in(window, OUT_DIR):
    """Return the energy consumed during the time window (in Joules)."""
    # The problem is that batsim time series don't always have the specific
    # timestamp, we need to extrapolate
    data = pandas.read_csv(OUT_DIR + "/_consumed_energy.csv")
    energy = [0, 0]
    for i in range(2):
        first_greater = data[data['time'].ge(window[i])].index[0]
        df_entry = data.iloc[first_greater]
        energy[i] = df_entry['energy']
        delta_energy = (df_entry['time'] - window[i]) * df_entry['epower']
        energy[i] -= delta_energy

    return energy[1] - energy[0]


def energy_consumed_in_windows(window_list, out_dir):
    energy_array = np.zeros(len(window_list))
    i = 0
    for window in window_list:
        energy_window = energy_consumed_in(window, out_dir)
        energy_array[i] = energy_window
        i += 1
    return energy_array

def time_length_windows(window_list) :
    length = 0
    for window in window_list :
        begin = window[0]
        end = window[1]
        length += (end-begin)
    return length

def energy_consumed_in_windows_fast(window_list, out_dir):
    data = pandas.read_csv(out_dir + "/_consumed_energy.csv")
    energy_array = np.zeros(len(window_list))
    energy = [0, 0]
    j = 0
    for window in window_list:
        for i in range(2):
            first_greater = data[data['time'].ge(window[i])].index[0]
            df_entry = data.iloc[first_greater]
            energy[i] = df_entry['energy']
            delta_energy = (df_entry['time'] - window[i]) * df_entry['epower']
            energy[i] -= delta_energy
        energy_total = (energy[1] - energy[0])
        energy_array[j] = energy_total
        j += 1
    return energy_array
