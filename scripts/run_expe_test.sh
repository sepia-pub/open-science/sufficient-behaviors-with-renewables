#!/bin/bash
# example launch of experiments with 3 days, 1 replicate and 2 effort scenarii (low and max)
python3 campaign.py --nb-replicat 1 --expe-range 0 --window-mode 8 --nb-days 3 --threads 2 \
--json-behavior behavior_file/low_effort.json behavior_file/max_effort.json  --compress-mode \
--production-file data_energy/energy_trace_sizing_solar.csv data_energy/energy_trace_sizing_solar.csv
