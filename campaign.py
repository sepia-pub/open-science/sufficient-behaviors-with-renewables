#!/usr/bin/env python3
import os
# from time import *
import datetime
import argparse
from typing import Dict

import psutil
from dateutil import parser
import concurrent.futures

from scripts.util import WL_DIR, ROOT_DIR, ExpeDict, create_dir_rec_if_needed, generate_dict_from_json
from scripts.generate_file import save_dict_to_json
from instance import start_instance, prepare_input_data, generate_windows_dict, \
    compress_expe_result, user_type_to_behavior
from scripts.stat_tools import plot_exec_time, plot_queue_load
from compute_metrics_campaign import compute_metrics_all_expe_parr


def parse_argument():
    """ parsing of command-line arguments"""
    argument_parser = argparse.ArgumentParser(
        description="run instance for test with multibehavior batmen user class and metacentrum workload")

    argument_parser.add_argument("--nb-replicat", type=int,
                                 help="number of different seed we will have to choose for the experiment (default 1)",
                                 default=1, required=False)
    argument_parser.add_argument("--threads", type=int,
                                 help="the number of threads the program will use (default : number of physical "
                                      "core)", default=psutil.cpu_count(logical=False), required=False)
    argument_parser.add_argument("--perf-record", action="store_true",
                                 help="Enable performance recording of the experiment using perf tool make sure it is "
                                      "install in the current shell else it will crash")
    argument_parser.add_argument("--perf-rate", type=int,
                                 help="the rate of perf sampling (default : 60 samples per second)", default=60,
                                 required=False)
    argument_parser.add_argument("--no-monolithic-test", help="Disable monolithic behavior test", action="store_true")
    argument_parser.add_argument("--no-prepare-workload",
                                 help="Disable computation of workload (useful if already computed before)",
                                 action="store_true")
    argument_parser.add_argument("--compress-mode",
                                 help="compute statistics during computation",
                                 action="store_true")
    argument_parser.add_argument("--production-files",
                                 help="provide the list of data for energy file "
                                      "(first one is for solar and solar_wind, second for simulated data)",
                                 default=["data_energy/data_odre.csv", "data_energy/energy_trace.csv"],
                                 nargs=2, required=False)
    argument_parser.add_argument("--expe-range", type=int,
                                 help="provide the range of expe to do",
                                 nargs='*', required=True)
    argument_parser.add_argument("--window-mode", type=int,
                                 help="specify the window_mode using bitwise and "
                                      "(first bit for default mode, 2nd for solar mode, 3rd for solar_wind mode, "
                                      "4th for simu_mode), (default : 15, all mode enabled)",
                                 default=15, required=False)
    argument_parser.add_argument("--custom-batmen-exec", type=str,
                                 help="allow to specify the path for custom batmen executable, by default use the "
                                      " batmen in the shell",
                                 default="batmen", required=False)
    argument_parser.add_argument("--out-dir", type=str,
                                 help="path where to output experiment results",
                                 default=f"{ROOT_DIR}/out", required=False)
    argument_parser.add_argument('--nb-days', type=int,
                                 help="the number of the days the experiment will last (for example with 9 we will "
                                      "have 1 heating days and 1 ending days and 7 day that we will do stat on )",
                                 default=9, required=False)
    argument_parser.add_argument("--json-behavior", type=str,
                                 help="path to the behavior json file", nargs="*",
                                 required=True)
    args = argument_parser.parse_args()
    print(args)
    return args


def get_behavior_dict(json_behavior):
    """generate behavior_prob_dict behavior_name_list and monolithic_behavior_list """
    behavior_prob_dict: Dict[str, Dict[any, any]] = {"rigid": {},
                                                     "renonce": {"red_prob_renonce_mono_core": 1.0,
                                                                 "red_prob_renonce_multi_core": 1.0},
                                                     "see_you_later": {"red_prob_see_you_later_mono_core": 1.0,
                                                                       "red_prob_see_you_later_multi_core": 1.0},
                                                     "reconfig": {"red_prob_reconfig_multi_core": 1.0,
                                                                  "red_prob_rigid_mono_core": 1.0},
                                                     "degrad": {"red_prob_degrad_multi_core": 1.0,
                                                                "red_prob_degrad_mono_core": 1.0}}
    monolithic_behavior_list = ["rigid", "renonce", "reconfig", "degrad"]
    behavior_name_list = [path.split("/")[-1].split(".")[0] for path in json_behavior]
    for behavior_name, behavior_file in zip(behavior_name_list, json_behavior):
        behavior_prob_dict[behavior_name] = generate_dict_from_json(behavior_file)
    return behavior_prob_dict, behavior_name_list, monolithic_behavior_list


def get_window_mode(window_mode_arg):
    """generate window_mode_list from argument given"""
    window_mode_list_option = ["", "_solar", "_solar_wind", "_simu_solar_wind", "_all_red"]
    window_mode_list = []
    modulus = 1
    for i in range(len(window_mode_list_option)):
        enabled = modulus & window_mode_arg
        if enabled:
            window_mode = window_mode_list_option[i]
            window_mode_list.append(window_mode)
        modulus = 2 * modulus
    return window_mode_list


def get_seed_to_launch_expe(expe_num: int, window_mode_nb: int, behavior_nb: int, seed: int, color_nb: int):
    seed_to_return = seed
    # We authorize a max of 64 experiments
    seed_expe_inter = (2 ** 27)
    seed_to_return += expe_num * seed_expe_inter
    # We authorize a max of 64 window_mode by experiments
    seed_window_mode_list_inter = (2 ** 21)
    seed_to_return += seed_window_mode_list_inter * window_mode_nb
    # We authorize a max of 64 different behavior to try
    seed_behavior_inter = (2 ** 16)
    seed_to_return += behavior_nb * seed_behavior_inter
    # We authorize at most 2^14 replicats and 4 window red and yellow combinations
    color_switch_inter = (2 ** 14)
    seed_to_return += color_nb * color_switch_inter
    return seed_to_return

def compute_expe_start_time(args) :
    begin_trace = 1356994806  # according to original SWF header
    jun1 = parser.parse('Sun Jun  1 00:00:00 CEST 2014')
    last_day_expe = parser.parse('Tue Nov 11 00:00:00 CEST 2014')


    # We do one expe for every week beween Jun 1 and Oct 26
    nb_days = args.nb_days
    heating_days = 1
    ending_days = 1
    begin_time = heating_days * 24 * 3600
    end_time = nb_days * 24 * 3600 - ending_days * 24 * 3600

    # the time between each simulation we try to keep the same alignment with weeks
    inter_day = max(((nb_days - heating_days - ending_days) // 7) * 7,7)
    expe_start_time = []
    day = datetime.timedelta(days=1)
    betweentime = inter_day * day
    str_start_day = []
    day1 = jun1
    while day1 <= (last_day_expe - betweentime):
        str_start_day.append(day1.ctime())
        expe_start_time.append(int(day1.timestamp()) - begin_trace)
        day1 += betweentime
    print(expe_start_time)
    # Save selected dates in a txt file
    with open(f"{WL_DIR}/start_days_for_campaign.txt", 'w') as f:
        for date in str_start_day:
            f.write(date + '\n')
    return expe_start_time,begin_time,end_time,heating_days


def run_expe(args,expe_start_time,begin_time: int,end_time: int,heating_days : int):
    nb_days = args.nb_days
    compress_mode = args.compress_mode
    nb_replicat = args.nb_replicat
    perf_record = args.perf_record
    perf_rate = args.perf_rate
    expe_range = []
    if not (args.expe_range is None):
        expe_range = args.expe_range
    if expe_range == [-1]:
        expe_range = range(len(expe_start_time))
    start_seed = 0

    behavior_prob_dict, behavior_name_list, monolithic_behavior_list = get_behavior_dict(args.json_behavior)
    window_mode_list = get_window_mode(args.window_mode)
    expe_done_dict = ExpeDict(behavior_name_list, expe_range, window_mode_list, ["red", "yellow_red"])
    print(
        f"Run expes {expe_range} with {nb_replicat} replicats on modes {window_mode_list} on behavior {behavior_name_list}")
    production_files = args.production_files
    # Number of day between Jan 1st and  Jun 1st
    start_day = 150
    yellow_threshold = 42 * 150
    red_threshold = yellow_threshold / 2
    red_windows_dict, yellow_windows_dict = generate_windows_dict(production_files=production_files,
                                                                  expe_range=expe_range,
                                                                  window_mode_list=window_mode_list,
                                                                  nb_days=nb_days - heating_days,
                                                                  heating_days=heating_days,
                                                                  start_day=start_day,
                                                                  red_threshold=red_threshold,
                                                                  yellow_threshold=yellow_threshold)
    out_dir = args.out_dir
    create_dir_rec_if_needed(out_dir)
    expe_dict_save_prefix = f"{out_dir}/expe_done_dict_"
    save_num = 0
    savefile = f"{expe_dict_save_prefix}{save_num}.json"
    while os.path.exists(savefile):
        save_num += 1
        savefile = f"{expe_dict_save_prefix}{save_num}.json"
    windows_dict: dict = {"red_windows": red_windows_dict, "yellow_windows": yellow_windows_dict}
    save_windows_file: str = f"{out_dir}/windows_dict_{save_num}.json"
    save_dict_to_json(windows_dict, save_windows_file)
    batmen_exec = args.custom_batmen_exec
    monolithic_test = not args.no_monolithic_test
    maximum_worker = args.threads
    prepare_work = not args.no_prepare_workload

    # Preparation of workload
    if prepare_work:
        with concurrent.futures.ProcessPoolExecutor(max_workers=maximum_worker) as executor:
            instances = []
            for i in expe_range:
                #  prepare_input_data(expe_num, start_date,nb_days)
                instances.append(executor.submit(prepare_input_data, i, expe_start_time[i], nb_days))
            for instance in concurrent.futures.as_completed(instances):
                print(f"Expe {instance.result()} teminated")

    # parallel launch of the experiments
    with concurrent.futures.ProcessPoolExecutor(max_workers=maximum_worker) as executor:
        instances = []
        counter_expe_done = 0
        for expe_nb in expe_range:
            print(f"Submit expe {expe_nb}")
            # start_instance(expe_num=expe_nb, start_date=expe_start_time[expe_nb],
            #                nb_days=nb_days, seed=0, red_windows=red_windows,
            #                yellow_windows=yellow_windows, out_dir=out_dir, behavior_prob={} ,
            #                batmen_exec="batmen", prepare_workload=True,
            #                clean_log=False, window_mode=None,
            #                perf_eval=False,perf_rate=60)

            for window_nb, window_mode in enumerate(window_mode_list):
                red_windows, yellow_windows = red_windows_dict[expe_nb][window_mode], yellow_windows_dict[expe_nb][window_mode]
                if monolithic_test:
                    for mono_behavior in monolithic_behavior_list:
                        instances.append(executor.submit(start_instance, expe_nb, expe_start_time[expe_nb],
                                                         nb_days, 0, red_windows,
                                                         [], out_dir, behavior_prob_dict[mono_behavior],
                                                         batmen_exec, False,
                                                         True, window_mode, mono_behavior,
                                                         perf_record, perf_rate))
                for j in range(start_seed, start_seed + nb_replicat):
                    # only red_windows expe
                    for behavior_nb, behavior in enumerate(behavior_name_list):
                        seed_red = get_seed_to_launch_expe(expe_nb, window_nb, behavior_nb, j, 0)
                        seed_yellow = get_seed_to_launch_expe(expe_nb, window_nb, behavior_nb, j, 1)
                        instances.append(executor.submit(start_instance, expe_nb, expe_start_time[expe_nb],
                                                         nb_days, seed_red,
                                                         red_windows,
                                                         [], out_dir, behavior_prob_dict[behavior],
                                                         batmen_exec, False,
                                                         True, window_mode, behavior,
                                                         perf_record, perf_rate))
                        # red_windows and yellow_windows expe
                        instances.append(executor.submit(start_instance, expe_nb, expe_start_time[expe_nb],
                                                         nb_days, seed_yellow,
                                                         red_windows,
                                                         yellow_windows, out_dir, behavior_prob_dict[behavior],
                                                         batmen_exec, False,
                                                         True, window_mode, behavior,
                                                         perf_record, perf_rate))

        for instance in concurrent.futures.as_completed(instances):
            (xp_num, seed, expe_time, expe_dir, is_yellow, user_type, window_mode) = instance.result()
            is_monolithic = user_type in monolithic_behavior_list
            if not is_yellow and not is_monolithic:
                expe_done_dict.add_expe(user_type, xp_num, window_mode, "red", seed)
            if is_yellow and not is_monolithic:
                expe_done_dict.add_expe(user_type, xp_num, window_mode, "yellow_red", seed)
            expe_time = int(expe_time)
            counter_expe_done += 1
            if counter_expe_done >= 1:
                counter_expe_done = 0
                expe_done_dict.to_json(json_filename=savefile)
            if compress_mode:
                behavior = user_type_to_behavior(user_type)
                if is_yellow:
                    window_mode += "_yellow"
                compress_expe_result(xp_num, behavior, seed, window_mode, red_windows, yellow_windows, begin_time,
                                     end_time, False, out_dir, destroy=False,energy_file=production_files[1],offset_production_day=start_day)
            print(
                f"\033[92m Expe {xp_num} with seed {seed}  terminated took {expe_time // 60}m{expe_time % 60}s \033[0m")

    # End of experiment computation of stats
    expe_done_dict.to_json(json_filename=savefile)
    return expe_done_dict, red_windows_dict, yellow_windows_dict, save_num, begin_time, end_time


def compute_metrics(args, expe_done_dict, red_windows_dict, yellow_windows_dict,
                    begin_time, end_time, offset_production_data, save_num,):
    monolithic_test = not args.no_monolithic_test
    maximum_worker = args.threads
    production_files = args.production_files
    start_day = offset_production_data
    out_dir = args.out_dir
    metrics, metrics_relative = compute_metrics_all_expe_parr(expe_done_dict, begin_time, end_time,
                                                              red_windows_dict, yellow_windows_dict, monolithic_test,
                                                              True,
                                                              maximum_worker, out_dir,
                                                              {"_simu_solar_wind": production_files[1]}, start_day)
    metrics_file_name = f"{out_dir}/metrics_campaign_{save_num}.csv"
    metrics.to_csv(metrics_file_name)
    metrics_relative.to_csv(f"{out_dir}/metrics_relative_campaign_{save_num}.csv")

    plot_exec_time(expe_done_dict, WL_DIR, out_dir, f'{out_dir}/execution_times_{save_num}.svg')
    plot_queue_load(expe_done_dict, out_dir)


def main():
    args = parse_argument()
    expe_start_time,begin_time,end_time,heatings_days = compute_expe_start_time(args)
    start_time_compute = datetime.datetime.now()
    print(f"Starting experiment at {start_time_compute}")
    expe_done_dict, red_windows_dict, yellow_windows_dict, save_num, begin_time, end_time = run_expe(args,expe_start_time,begin_time,end_time,heatings_days)
    end_time_compute = datetime.datetime.now()
    print("Experiments took " + str(end_time_compute - start_time_compute))
    compute_metrics(args, expe_done_dict, red_windows_dict, yellow_windows_dict, begin_time, end_time, 150, save_num)


if __name__ == "__main__":
    main()
