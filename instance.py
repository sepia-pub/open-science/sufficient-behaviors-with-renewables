#!/usr/bin/env python3

import time
import argparse
from typing import List, Dict

import pandas as pd

import batmenTools.swf2batsim_split_by_user as split_user
import batmenTools.swf_filter as split_time
from scripts.util import *
from scripts.stat_tools import compute_metrics


def prepare_input_data(expe_num: int, start_date: int, nb_days: int):
    """Cut the original trace to extract nb_days days starting from this start date"""
    end_date = start_date + nb_days * 24 * 3600
    to_keep = f"submit_time >= {start_date} and submit_time <= {end_date}"
    swf_to_filter = f'{WL_DIR}/MC_selection_article.swf'
    swf_expe = f"{WL_DIR}/MC_selection_expe{expe_num}.swf"
    expe_dir = f"{WL_DIR}/expe{expe_num}"
    if not os.path.exists(expe_dir):
        os.makedirs(expe_dir)
    split_time.filter_workload(input_swf=swf_to_filter, output_swf=swf_expe, keep_only=to_keep)
    split_user.generate_workload(
        input_swf=swf_expe,
        output_folder=expe_dir,
        job_grain=10,
        job_walltime_factor=8)
    return expe_num, start_date, end_date


def compress_expe_result(exp_num: int, behavior: str, seed: int, window_type: str, red_windows: List[List[int]],
                         yellow_windows: List[List[int]], begin_time: int, end_time: int,
                         sanity_check: bool, out_dir: str,energy_file,offset_production_day, destroy: bool = True,):
    expe_dir = f"{out_dir}/expe{exp_num}/{behavior}{window_type}_{seed}"
    out = compute_metrics(exp_num, behavior, seed, window_type, red_windows, yellow_windows, begin_time, end_time,
                          sanity_check, out_dir,energy_file,offset_production_day)
    data = pd.DataFrame(data=out, index=[0])

    stat_filename = f"{expe_dir}/log/condensed_stat.csv"
    data.to_csv(stat_filename)
    if destroy:
        os.remove(f"{expe_dir}/_jobs.csv")
        os.remove(f"{expe_dir}/_machine_states.csv")
        os.remove(f"{expe_dir}/user_stats_behaviors.csv")
        os.remove(f"{expe_dir}/_consumed_energy.csv")
        os.remove(f"{expe_dir}/_schedule.trace")


def run_expe(expe_num: int, user_category, behavior_prob: Dict[any, any], red_windows: List[List[int]],
             yellow_windows: List[List[int]], seed: int, user_type: str,
             clean_log: bool, batmen_exec: str, socket_offset: int,
             filename: str, perf_eval: bool, perf_rate: int, out_dir: str, nb_cores: int):
    """Run batmen with given behavior and demand response window.
    Expe_num should be a small integer (eg < 100)"""
    # Useful vars and output folder
    begin_expe = time.time()
    if filename is None:
        filename = ""
    expe_dir = f"{out_dir}/expe{expe_num}/{user_category}{filename}"
    if yellow_windows:
        expe_dir += "_yellow"

    expe_dir += "_" + str(seed)
    create_dir_rec_if_needed(expe_dir)
    create_dir_rec_if_needed(f"{expe_dir}/cmd")
    expe_file = f"{expe_dir}/cmd/robinfile.yaml"
    wl_folder = f'{WL_DIR}/expe{expe_num}'
    pf = f"{ROOT_DIR}/platform/average_metacentrum.xml"
    wl = f"{WL_DIR}/empty_workload.json"
    uf = f"{expe_dir}/cmd/user_description_file.json"

    # User description file
    def user_description(user, user_category_name, probabilities : Dict[any,any]):
        input_dict = {"input_json": f"{wl_folder}/{user}.json"}
        param_dict = input_dict | probabilities
        return {
            "name": user,
            "category": user_category_name,
            "param": param_dict
        }

    user_names = [user_file.split('.')[0] for user_file in os.listdir(wl_folder)]
    data = {}
    if red_windows:
        data["red_windows"] = red_windows
    data["seed"] = seed
    if yellow_windows:
        data["yellow_windows"] = yellow_windows
    data["log_user_stats"] = True
    data["log_folder"] = expe_dir
    data["users"] = [user_description(user, user_category, behavior_prob) for user in user_names]
    with open(uf, 'w') as user_description_file:
        json.dump(data, user_description_file, indent=1)

    # Generate and run robin instance
    socket_batsim = f"tcp://localhost:28{socket_offset:03d}"
    socket_batsched = f"tcp://*:28{socket_offset:03d}"
    quiet_batsim = ""
    quiet_batsched = ""
    perf_eval_prefix = ""
    if perf_eval:
        perf_eval_prefix = f"perf record -F {perf_rate} -g -o {expe_dir}/log/perf.data"
    if clean_log:
        quiet_batsim = "-q "
        quiet_batsched = "--verbosity=silent "
    batcmd = gen_batsim_cmd(
        pf, wl, expe_dir,
        f"--socket-endpoint={socket_batsim} {quiet_batsim} --energy --enable-compute-sharing --enable-dynamic-jobs "
        f"--acknowledge-dynamic-jobs --enable-profile-reuse")
    schedcmd = f"{perf_eval_prefix} {batmen_exec} {quiet_batsched} --nb_cores={nb_cores} --socket-endpoint={socket_batsched} -v bin_packing_energy  " \
               f"--variant_options_filepath={uf}"
    instance = RobinInstance(output_dir=expe_dir,
                             batcmd=batcmd,
                             schedcmd=schedcmd,
                             simulation_timeout=604800, ready_timeout=10,
                             success_timeout=3600, failure_timeout=5
                             )
    instance.to_file(expe_file)
    print(f"Run robin {expe_file}")
    run_robin(expe_file)
    print(f"Robin {expe_file} finished")
    end_exp = time.time()
    filename = expe_dir + "/log/python-log.log"
    print(filename)
    full_expe_time = int(end_exp - begin_expe)
    with open(filename, "w+") as logfile:
        logfile.write(
            f"{expe_num}\n{seed}\n{full_expe_time}\n"
            f"Expe {expe_num} with seed {seed}  terminated took {full_expe_time // 3600}h{(full_expe_time % 3600) // 60}"
            f"m{full_expe_time % 60}s")

    return expe_dir
    # Remove the log files that can quickly become heavy...
    # if clean_log:
    # os.remove(f"{expe_dir}/log/batsim.log")
    # os.remove(f"{expe_dir}/log/sched.err.log")
    # os.remove(f"{expe_dir}/log/sched.out.log")





def intersect_windows(window1: List[int], window2: List[int]):
    disjoint = window1[0] > window2[1] or window2[0] > window1[1]
    return not disjoint


def make_windows_disjoint(red_windows_sorted: List[List[int]], yellow_windows_sorted: List[List[int]]):
    i = 0
    j = 0
    while i < len(red_windows_sorted) and j < len(yellow_windows_sorted):
        red_window = red_windows_sorted[i]
        yellow_window = yellow_windows_sorted[j]
        if intersect_windows(red_window, yellow_window):
            if red_window[0] > yellow_window[0]:
                red_window[0] = yellow_window[1] + 1
                j += 1
            else:
                yellow_window[0] = red_window[1] + 1
                i += 1
        else:
            if red_window[0] > yellow_window[0]:
                j += 1
            else:
                i += 1


def simulated_window_from_csv_file_auto(production_file: str, start_exp: int, end_exp: int):
    """ compute the window from a csv file with header time,power
    start_exp and end_exp are the hours of start and end of the experiments"""
    data_prod = pandas.read_csv(production_file)
    energy_max = data_prod["power"].mean() * (100 / 110)
    red_windows, yellow_windows = compute_window_from_data(data_prod, energy_max, energy_max/2, start_exp, end_exp)
    make_windows_disjoint(red_windows, yellow_windows)
    return red_windows, yellow_windows

def simulated_window_from_csv_file_threshold(production_file : str, yellow_threshold,
                                             red_threshold,start_exp: int ,end_exp : int) :
    """ compute the window from a csv file with header time,power
    start_exp and end_exp are the hours of start and end of the experiments"""
    data_prod = pandas.read_csv(production_file)
    red_windows, yellow_windows = compute_window_from_data(data_prod, yellow_threshold=yellow_threshold,
                                                         red_threshold=red_threshold, start_exp=start_exp, end_exp=end_exp)
    make_windows_disjoint(red_windows, yellow_windows)
    return red_windows, yellow_windows
def compute_window_from_data(data_prod: pd.DataFrame, yellow_threshold,red_threshold, start_exp: int, end_exp: int):
    state = 2
    begin, end = start_exp * 3600, end_exp * 3600
    interval = [begin, 0]
    data_prod_exp = data_prod[data_prod.time >= begin]
    data_prod_exp = data_prod_exp[data_prod_exp.time <= end]
    data_prod_exp["time"] = data_prod_exp["time"] - begin
    data_prod_exp = data_prod_exp.sort_values(["time"], ignore_index=True)
    # windows[0] contains the red windows and windows[1] contains the yellow windows
    windows: List[List[List[int]]] = [[], []]
    for i in range(len(data_prod_exp)):
        production = data_prod_exp["power"][i]
        interval[1] = int(data_prod_exp["time"][i])
        if production > yellow_threshold:
            if state != 2:
                windows[state].append(interval.copy())
                state = 2
        elif production < red_threshold:
            if state != 0:
                if state == 1:
                    windows[state].append(interval.copy())
                state = 0
                interval[0] = int(data_prod_exp["time"][i])
        else:
            if state != 1:
                if state == 0:
                    windows[state].append(interval.copy())
                state = 1
                interval[0] = int(data_prod_exp["time"][i])
    return windows[0].copy(), windows[1].copy()


def generate_windows_dict(production_files: List[str], expe_range: List[int], window_mode_list: List[str], nb_days: int,
                          heating_days: int, start_day: int,red_threshold,yellow_threshold):
    """Precompute every red and yellow windows needed for the experiment and store them in a dict"""
    red_windows_dict = {}
    yellow_windows_dict = {}
    for xp in expe_range:
        red_windows_dict[xp] = {}
        yellow_windows_dict[xp] = {}
        for window_mode in window_mode_list:
            red_windows, yellow_windows = [], []
            begin_day = start_day + heating_days + xp * (nb_days - heating_days)
            end_day = begin_day + nb_days
            if window_mode == "":
                red_windows, yellow_windows = basic_windows(nb_days)
            elif window_mode == "_solar_wind":
                red_windows, yellow_windows = simulated_window_from_csv_file_threshold(production_files[0],
                                                                                       yellow_threshold=yellow_threshold,
                                                                                       red_threshold=red_threshold,
                                                                                       start_exp=begin_day * 24,
                                                                                       end_exp=end_day * 24)
            elif window_mode == "_simu_solar_wind":
                red_windows, yellow_windows = simulated_window_from_csv_file_threshold(production_files[1],
                                                                                       yellow_threshold=yellow_threshold,
                                                                                       red_threshold=red_threshold,
                                                                                       start_exp=begin_day * 24,
                                                                                       end_exp=end_day * 24)
            elif window_mode == "_all_red":
                red_windows = [[0, (nb_days - heating_days) * 3600]]
            shift_windows(heating_days * 24 * 3600, red_windows)
            shift_windows(heating_days * 24 * 3600, yellow_windows)
            red_windows_dict[xp][window_mode] = red_windows
            yellow_windows_dict[xp][window_mode] = yellow_windows
    return red_windows_dict, yellow_windows_dict





def basic_windows(nb_days: int):
    hour = 3600
    red_windows = [[0, 7 * hour]]
    yellow_windows = []
    for i in range(nb_days):
        offset = i * (24 * hour)
        red_windows += [[offset + 19 * hour, offset + ((24 + 7) * hour)]]
        yellow_windows += [[offset + 7 * hour, offset + 9 * hour], [offset + 17 * hour, offset + 19 * hour]]
    return red_windows, yellow_windows


def shift_windows(time: int, windows_list: List[List[int]]):
    """ shift the windows in windows_list by a time amount of seconds """
    for window in windows_list:
        window[0] += time
        window[1] += time


def start_instance(expe_num: int, start_date: int, nb_days: int, seed: int, red_windows: List[List[int]],
                   yellow_windows: List[List[int]], out_dir: str, behavior_prob: Dict[any, any],
                   batmen_exec: str = "batmen",
                   prepare_workload: bool = True, clean_log: bool = False, window_mode=None, user_type: str = "",
                   perf_eval: bool = False, perf_rate: int = 60, nb_cores: int = 18):
    start_time = time.time()
    # Prepare workload
    if prepare_workload:
        prepare_input_data(expe_num, start_date, nb_days)

    # Create expe folder
    create_dir_rec_if_needed(out_dir)
    suffix = ""
    socket_offset = os.getpid() % 999 + 1
    if window_mode:
        suffix = window_mode
    if user_type != "rigid" and user_type != "multi":
        suffix = "_" + user_type + suffix
    # Run with Rigid behavior (the demand response window has no influence here)
    if user_type == "rigid":
        expe_dir = run_expe(expe_num=expe_num,
                            user_category="replay_user_rigid",
                            behavior_prob=behavior_prob,
                            red_windows=red_windows, yellow_windows=yellow_windows,
                            seed=0,
                            user_type="rigid",
                            filename=suffix,
                            socket_offset=socket_offset,
                            clean_log=clean_log,
                            perf_eval=perf_eval,
                            perf_rate=perf_rate,
                            batmen_exec=batmen_exec,
                            out_dir=out_dir,
                            nb_cores=nb_cores)
    else:
        expe_dir = run_expe(expe_num=expe_num, user_category="dm_user_multi_behavior", behavior_prob=behavior_prob,
                            red_windows=red_windows, yellow_windows=yellow_windows, seed=seed,
                            user_type=user_type, filename=suffix, socket_offset=socket_offset, clean_log=clean_log,
                            perf_eval=perf_eval, perf_rate=perf_rate, batmen_exec=batmen_exec, out_dir=out_dir,
                            nb_cores=nb_cores)

    ###### Output data treatment ######
    # Produce the utilisation viz?
    end_time = time.time()

    return expe_num, seed, end_time - start_time, expe_dir, yellow_windows != [], user_type, window_mode


def user_type_to_behavior(user_type: str):
    if user_type == "rigid":
        return "replay_user_rigid"
    elif user_type == "multi":
        return "dm_user_multi_behavior"
    else:
        return "dm_user_multi_behavior_" + user_type


def main():
    pass
    parser = argparse.ArgumentParser(
        description='One expe instance. To launch for example with `oarsub -l walltime=2 "./1_one_instance arg1 arg2 '
                    'arg3"`')
    parser.add_argument('expe_num', type=int, help='The expe ID')
    parser.add_argument('start_date', type=int,
                        help='Start of the"  ' + '-day window (in seconds since the start of the original trace)')
    args = parser.parse_args()

    start_instance(args.expe_num, args.start_date, args.nb_days, 0, [[0, 1]], [], "multi")


if __name__ == "__main__":
    pass
    # main()
