# Sufficiency behaviors under renewable production for data center users 

This repository is the artifact associated with the article
J. Gatt, M. Madon, G. Da Costa
*"Digital sufficiency behaviors to deal with intermittent energy sources in a data center"*,
published at the conference ICT4S'24.
It contains all the scripts and files needed to reproduce the experiments that are described.

It is originally a fork from [demand-response-user](https://gitlab.irit.fr/sepia-pub/open-science/demand-response-user). 

## Description of the main files
- folders `behavior_file/`, `data_energy/`, `platform/` and `workload/`: contain the simulation inputs
- `default.nix`: [Nix](https://nixos.org/) file defining the software dependencies and their specific versions
- `scripts/prepare_workload.sh`: script to download and filter the input workload
- `scripts/run_expe.sh`: script to launch the full experimental campaign
(with the help of `campaign.py`, preparing and launching in parallel the experiments,
each being one instance of `instance.py`)
- `scripts/compute_stat.sh` script to compute statistics on the experiment outputs
- `analyse_campaign.ipynb`: Jupyter notebook analyzing the results and plotting the graphs


## Steps to reproduce the experiments

### 1. Install dependencies (install Nix + ~4 minutes)

The main software used for our simulations are:
- [Batsim](https://batsim.org/) v4.10 and [SimGrid](https://simgrid.org/) v3.31 for the infrastructure simulation
- [Batmen](https://gitlab.irit.fr/sepia-pub/mael/batmen) v2.0: our set of schedulers for [Batsim](https://batsim.org/) and plugin to simulate users
- [Batmen-tools](https://gitlab.irit.fr/sepia-pub/mael/batmen-tools/-/tree/main/) : a set of tools to manipulate swf files
- python3, pandas, jupyter, matplotlib etc. for the data analysis


All the dependencies of the project and their specific versions are handled by the package manager [Nix](https://nixos.org/).
We highly recommend you to use it if you want to reproduce the experiments on your machine.
If you do not have [Nix](https://nixos.org/) installed, follow the instructions provided on their [installation page](https://nixos.org/download/#download-nix).

Once the installation is done, enter a nix-shell where all the dependencies are available. 
This will download, compile and install them, which can take some time:

```bash
nix-shell -A exp_env --pure
```

### 2. Prepare input workload (~ 2 minutes)
Inside the nix-shell, run the following script to download (from the [Parallel Workloads Archive](https://www.cs.huji.ac.il/labs/parallel/workload/)) and filter the input workload used in the experiments:

```bash
scripts/prepare_workload.sh
```

### 3. Launch campaign (~7 hours)
The experimental campaign consists of
- preparing the inputs for each instance (random seed, energy state windows, workload, behavior scenario),
- running the simulations with [Batsim](https://batsim.org/) and Batmen, and
- computing aggregated metrics on all the simulation outputs.

The full experimental campaign described in the paper is quite long.
It took us 7 hours to complete in parallel on a 2x16 core Intel Xeon Gold 6130 machine.
For this reason, we provide two versions of the campaign:
- `scripts/run_expe.sh`: the campaign presented in the paper with 164 days, 4 effort scenarios and 30 replicates
- `scripts/run_expe_test.sh`: a small campaign with 3 days, 2 effort scenarios and 30 replicates *(runtime < 1 minute)*

Both of them take charge of calling `campaign.py` with the good parameters.
For example, for the full experimental campaign:

```bash 
python3 campaign.py --nb-replicat 30 --expe-range -1 --window-mode 8 --nb-days 164 \
--json-behavior behavior_file/big_effort.json behavior_file/low_effort.json behavior_file/max_effort.json behavior_file/medium_effort.json \
--compress-mode --production-file data_energy/energy_trace_sizing_solar.csv data_energy/energy_trace_sizing_solar.csv
```

For more information on the available parameters, run `python3 campaign.py --help`

As every experiment can take up to 20 GB of RAM, 
you might be limited by the memory of your system.
When you are running the experiments you can limit 
the number of parallel run using `--threads n` command-line argument 
with `n` the maximum of experiments to run in parallel.
By default, it uses every physical cores available.



### 4. Data analysis (~ 1 minute)

From the aggregated metrics provided by the previous steps, we can plot the graphs presented in the paper.
Everything is explained in the notebook `analyse_campaign.ipynb`.
You will have to change the variables defined in the first cell to match with your setup.

Still from inside the nix-shell, you can run the notebook with 
```bash
jupyter notebook
```


Given the time required to reproduce the full experimental campaign,
we provide the campaign's aggregated metrics in the files `out/metrics_fullexpe.csv` and `out/metrics_relative_fullexpe.csv`.
Note that these metrics were obtained with a previous version of the campaign,
and might differ from the one you obtain. 
In our reproduction, we noticed a relative difference of 0.5% in energy related metrics and
2% in user behaviors related metrics



## Extra informations
### Tips 
With the experiments, there are various scripts_file provided 
to help you manage the data of experiments :
- `scripts/compress_out.sh out/ out.tar.zst`
allow you to compress the `out_dir` in a tar file for archive purposes. 
In our case, we divided by 7 the space used by the experiments result.
- `scripts/sync_expe_out.sh out/  path_to_backup/` allow you to do 
backup of simulation data in a backup directory. 
It uses rsync so it will only write changes if you do twice this command.

### Energy Data
The provided `example_trace_occitanie_2019.csv` comes from a modification of the Open Data Réseaux Electrique 
energy production dataset for Occitanie in 2019 (the original file can be directly downloaded 
[here](https://odre.opendatasoft.com/explore/dataset/eco2mix-regional-cons-def/information/?disjunctive.libelle_region&disjunctive.nature)).
The provided `energy_trace_sizing_solar.csv`
is the energy trace of the energy produced by DataZero2 sizing algorithm.

### Advanced options
Inside the nix-shell exp_env, launch the command `python3 campaign.py --help` to get the details of every possible arguments.

You will have to give at least the following arguments :
- `--expe-range` to provide the expe to do for all available 
experiment type `--expe-range -1` else provide the list of experiment 
with experiments `[0,1]` it will be `--expe-range 0 1`

### Information about experiments
The experiments took 7 hours to do on two Intel Xeon Gold 6130 and the output took 55 GB to store, 
once compress using `scripts/compress-out.sh` it takes 7.8 GB. 
As each experiments took approximately 20 GB of RAM and we had 188GB available we weren't able to exploit at full capacity the CPU, 
so with more memory we could have better speed.

### List of metrics 
The metrics computed for the experiments are the following (available in `result-big-expe`) :
- `XP`,`dir`, `behavior`, `seed`,
the experiment number in our case it is always 0, the directory from which the data where computed,
the behavior the user have (probability distribution can be found in behavior_file),
the seed the random generator used for computation.
- `#jobs` the number of jobs submitted, renounced jobs are not accounted in this total.
- `Duration_red (seconds)`, `Duration_yellow (seconds)`, `Duration_total (seconds)` 
the duration in which the red state occured, yellow state occured, the total measures were done
- `NRJ_red (Joules)`, `NRJ_yellow (Joules)`, `NRJ_total (Joules)` 
the energy consumed in red state, yellow state and during the whole duration of the experiments
- `mean_waiting_time`, `max_waiting_time`, `mean_slowdown`, `max_slowdown`.
The slowdown and waiting time metrics computed by batsim it doesn't take into account the user behavior
- `energy overproduced (Joules)`, `energy underproduced (Joules)`
Computation of the energy differences between consumption and production. 
When the energy produced is higher than the energy consumed, 
the absolute difference is added to overproduced energy.
When the energy produced is lower than the energy consumed, 
the absolute difference is added to overproduced energy.
- `energy balance (Joules)`, 
it is the computation of the difference between the energy produced and energy consumed.
It has been computed by two ways : using overproduction and underproduced (1), 
compute the total energy produced - total energy consumed (2). 
Relative Accuracy is the difference between both computation normalized.
- `true_rigid_jobs` is the number of jobs that was unmodified.
- `mean_delay`, `max_delay` 
the amount of time the jobs have been delayed by a see_you_later behavior
- `renonce_jobs`,`reconfig_jobs`,`degrad_jobs`,`rigid_jobs`
the number of jobs that was renounced, reconfigured, degraded, 
submitted rigidly (might have been delayed by see_you_later)
- `number_of_see_you_later`,`C_you_later_jobs`, 
the number of see_you_later (a jobs can be subject to more than one see_you_later), 
the number of jobs that get a see_you_later. 
It was computed using two ways, only looking for see_you_later in logged behavior (1), 
check if the original submission time is the real submission time in the jobs batsim record (2).
- `mean_corrected_wtime`, `max_corrected_wtime`, `mean_corrected_sdown`, `max_corrected_sdown`.
Compute the waiting time and slowdown while taking into account of behaviors 
(submission time is the original one without see you later, reconfig jobs execution time is the one before reconfiguring)
It has been computed by using behaviors stat and jobs data (1), or by crossing the data with the rigid case (2).
As the difference (sanity) are quite big we are not sure of the accuracy of these data computed.