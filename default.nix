# Reproductibility: pin a specific commit of nur-kapack repository
# This one contains Batsim v4.10 and Simgrid v3.31
{ kapack ? import (fetchTarball {
    url = "https://github.com/oar-team/nur-kapack/archive/b3611a0e7aadd69ca961d14639e8470df19f8564.tar.gz";
    sha256 = "sha256:0rjwq0csb1z79jakicl638xa8r1wwyf8vd8ymrpfa1waiw7vfpc5";
  }) {}
}:

with kapack.pkgs;

let self = rec {
  # For my scheduler and user simulator (batmen), pin a specific git commit:
  batmen = kapack.batsched.overrideAttrs (attr: rec {
    name = "batmen";
    version = "refs/tags/v2.0";
    src = fetchgit rec {
      url = "https://gitlab.irit.fr/sepia-pub/mael/batmen.git";
      rev = version;
      sha256 = "sha256-9lPEku1x8SbHn3cLwH9Gk6X4tDsWKVTKvUl9o4iGYo0=";
    };
  });

  batmen-tools = python3.pkgs.buildPythonPackage rec {
    pname = "batmenTools";
    version = "caefe1c12a059c919d2710ee8a00b9c179faf907";
    src = fetchgit {
      url = "https://gitlab.irit.fr/sepia-pub/mael/batmen-tools.git";
      rev = version;
      sha256 = "sha256-JDceMDCCTyQIYGMfnk964HrBkydky0vO7n8AYJs38xM=";
    };
    format = "pyproject";
    doCheck = false;
    buildInputs = [];
    checkInputs = [];
    nativeBuildInputs = [
      pkgs.python3Packages.flit-core
    ];
    propagatedBuildInputs = [
      pkgs.python3Packages.numpy
      pkgs.python3Packages.pandas
    ];
  };

  exp_env = mkShell rec {
    name = "exp_env";
    buildInputs = [
      kapack.batsim-410   # data center simulator
      batmen              # scheduler + user simulator
      kapack.batexpe      # tool to execute batsim instances
      kapack.evalys       # for data visualization
      (python3.withPackages 
        (ps: with ps; with python3Packages; [jupyter ipython pandas numpy psutil matplotlib plotly pip tabulate pytz isodate ordered-set yattag])
      )
      curl
      batmen-tools
      rsync # to synchronize directory
    ];
  };

    exp_env_extra = mkShell rec {
    name = "exp_env_extra";
    buildInputs = [
      exp_env.buildInputs
      flamegraph  #to create flame graph from perf data for bettar visualization
      zstd # to compress the result for storage purpose
    ];
  };
};
in
  self


